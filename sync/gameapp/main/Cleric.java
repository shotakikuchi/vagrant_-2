package gameapp.main;

import java.util.*;


public class Cleric {

    private String name;
    private int hp = 50;
    private int mp = 10;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {

        if(name = null){
            throw new IllegalArgumentException("名前がnullである。処理を中断");
        }else if(name.length() <= 1){
            throw new IllegalArgumentException("名前が短すぎる。処理を中断");
        }else if(name.length() >= 8){
            throw new IllegalArgumentException("名前が長すぎる。処理を中断");
        }


        this.name = name;
    }

    static final int MAX_HP = 50;
    static final int MAX_MP = 10;

    //引数が三つある場合のコンストラクタ
    public Cleric(String name, int hp, int mp) {
        this.name = name;
        this.hp = hp;
        this.mp = mp;
    }

    //引数が2つある場合のコンストラクタ
    public Cleric(String name, int hp) {
        this(name, hp, Cleric.MAX_MP);
    }

    public Cleric(String name) {
        this(name, Cleric.MAX_HP);
    }

    public void selfAid() {
        System.out.println(this.name + "はセルフエイド呪文を唱えた！");

        this.mp -= 5;
        this.hp = Cleric.MAX_HP;
    }

    public int pray(int sec) {
        System.out.println(this.name + "は" + sec + "秒間天に祈った");

        int recover = new Random().nextInt(3) + sec;

        int recoverAcrual = Math.min(Cleric.MAX_MP - this.mp, recover);

        this.mp += recoverAcrual;

        System.out.println("MPが" + recoverAcrual + "回復しました");

        return recoverAcrual;
    }
}