package gameapp.magic;

public class Wand{
    private String name;
    private double power;

    public void setName(String name) {

        if(name == null){
            throw new IllegalArgumentException("値を設定してください");
        }else if(name.length() <= 2){
            throw new IllegalArgumentException("2文字以上で設定してください");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPower(double power) {

        if(power < 5 || power > 100){
            throw new IllegalArgumentException("杖に設定されようとしている値が以上です");
        }

        this.power = power;
    }

    public double getPower() {
        return power;
    }
}