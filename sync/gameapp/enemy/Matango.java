package gameapp.enemy;
import gameapp.hero.Hero;

public class Matango {
    private int hp = 50;
    private char suffix;

    //Hp getter , setter
    public int getHp() {
        return hp;
    }
    public void setHp(int hp) {
        this.hp = hp;
    }

    public Matango(char suffix){
        this.suffix = suffix;
    }

    public void attack(Hero h){
        System.out.println("きのこ" + this.suffix + "の攻撃");

        System.out.println("10のダメージ");
        h.setHp(h.getHp() - 10);
    }

}