package gameapp.hero;
public class Hero {

    private String name;
    private int hp = 50;
    private int mp = 10;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {

        if (name == null) {
            throw new IllegalArgumentException("名前がnullである。処理を中断");
        } else if (name.length() <= 1) {
            throw new IllegalArgumentException("名前が短すぎる。処理を中断");
        } else if (name.length() >= 8) {
            throw new IllegalArgumentException("名前が長すぎる。処理を中断");
        }

        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getMp() {
        return mp;
    }

}