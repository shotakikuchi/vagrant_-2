package companyapp.property;
import companyapp.property.TangibleAsset;

public class Book extends TangibleAsset{

    protected String isbn;

    public Computer(String name,int price,String color,String isbn) {
        super(name,price,color);
        this.isbn = isbn;
    }

    //getterメソッド
    public String getIsbn() {
        return isbn;
    }
}
