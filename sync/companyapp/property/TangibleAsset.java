package companyapp.property;
import companyapp.property.Asset;
import companyapp.thing.Thing;

public abstract class TangibleAsset extends Asset implements Thing{

    protected String color;
    protected double weight;

    public TangibleAsset(String name,int price, String color){
        super(name, price);
        this.color = color;
    }

    //getterメソッド
    public String getColor() {
        return color;
    }

    public double getWeight() {
        return weight;
    }

    //setterメソッド
    public void setWeight(double weight) {
        this.weight = weight;
    }
}