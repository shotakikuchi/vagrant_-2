package companyapp.property;

import companyapp.property.TangibleAsset;

public class Computer extends TangibleAsset{

    protected String makerName;

    public Book(String name,int price,String color,String makerName) {
        super(name,price,color);
        this.makerName = makerName;
    }

    //geteerメソッド
    public String getMakerName() {
        return makerName;
    }

}