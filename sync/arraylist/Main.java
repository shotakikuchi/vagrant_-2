import arraylist.Hero;
import java.util.*;

public class Main{
    public static void Main(String[] args) {

        ArrayList<String> names = new ArrayList<String>();

        names.add("赤");
        names.add("青");
        names.add("黄");

        //通常のfor分
        for(int i = 0; i< names.size();i++){
            System.out.println(names.get(i));
        }


        Map<Hero,Integer> heros = new TreeMap<Hero,Integer>();
        heros.put("斎藤",3);
        heros.put("渡辺",7);

        for(Hero key: heros.keySet()){
            int value = heros.get(key);
            System.out.println(key.getName() + "が倒した敵の数=" + value);
        }

        //拡張for分
        for(String s : names){
            System.out.println(s);
        }


        Hero h1 = new Hero("斎藤");
        Hero h2 = new Hero("鈴木");

        List<Hero> Heros = new ArrayList<Hero>();

        Heros.add(h1);
        Heros.add(h2);

        for(int i = 0; i < Heros.size();i++){
            System.out.println(Heros.get(i).getName());
        }

        Map<Hero,Integer> heros = new TreeMap<Hero, Integer>();
        heros.put(h1, 3);
        heros.put(h2,7);

        for (Hero key:heros.keySet()){
            //値を取得
            int value = heros.get(key);

            System.out.println(key.getName() + "が倒した敵の数は" + value);
        }

    }
}