package arraylist;

public class Hero implements Cloneable{
    private String name;
    private int hp;
    private String sword;
    public Hero(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public  Hero clone(){
        Hero result = new Hero();
        result.name = this.name;

        //sharow copy
        result.hp = this.hp;

        //deep copy
        result.sword = this.sword.clone();
        return result
    }

    //Hero型を想定
    public boolean equals(Objet objet){
        if(objet == this){
            return true;
        }
        if(objet == null){
            return false;
        }
        if(!(objet instanceof Hero)){
            return false;
        }
        Hero r = (Hero) objet;

        //名前で比較を想定
        if(!this.getName().trim().equals(r.getName().trim())){
            return false;
        }
        return true;
    }

    public int hashCode(){
        int result = 37;
        result = result * 31 + this.name.hashCode();
        result = result * 31 + this.hp;
        return result;
    }
}
