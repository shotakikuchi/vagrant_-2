import java.uitl.*;

public class Book implements Comparable<Book>, Cloneable{
    private String title;
    private Date publishDate;
    private String comment;

    //getter / setterの宣言は省略

    public int hashCode(){
        int result = 1;
        result = result * 31 + this.title.hashCode();
        result = result * 31 + this.Date.hashCode();
        result = result * 31 + this.comment.hascCode();
        return result;
    }

    public boolean equals(Object object){
        if(this == object){
            return true;
        }

        if(object == null){
            return false;
        }

        if(!(object instanceof Book)){
            return false;
        }

        Book r = (Book) object;

        if((r.publishDate.equals(this.publishDate)) && r.title.equals(this.title)){
            return true;
        }
        return false;
    }

    public int compareTo(Book o){
        return(this.publishDate.compaTo(o.publishDate));
    }

    public Book clone(){
        Book result = new Book();
        result.title = this.title.clone();
        result.comment = this.comment.clone();
        result.publishDate = (Date) this.publishDate.clone();

        return result;
    }
}